# ptyknot
Copyright (c) 2016 Bart Massey

**This project has now moved to
[GitHub](http://github.com/BartMassey/ptyknot) because Rust
is there.*** Thanks much to GitLab for hosting it for the
last few weeks.

-----

This Rust library provides support for creating a child
process running a specified action, optionally with a new
pseudo-tty as its controlling terminal and with parent pipes
for some of its initial file descriptors. The caller gets
the master side of the pseudo-tty and pipes for
manipulation, along with the process ID of the child. The
caller can then later wait for the child to exit and examine
its exit status.

# Issues

* The name "ptyknot" is stupid.

* This library is quite Linux-specific.

  * It currently requires a Unix-98 `ptmx` pseudo-tty implementation:
    the older BSD-style pseudo-ttys are not yet supported.

  * It currently requires a SysV-style controlling terminal
    implementation that sets a controlling terminal on first
    tty open. The BSD-style `ioctl` to set a controlling
    terminal is not yet supported.

  * There is a comment in the Rust library source that
    indicates that this code will have memory-related
    crashes on some UNIX systems, probably because of broken
    `fork` or thread implementations.

* This code needs careful review. It's probably full of all
  kinds of badness.

# Notes

This code does not build with rustc 1.8 because of compiler
and library bugs.

# Credits

This work initially borrowed from
[tty-rs](http://github.com/stemjail/tty-rs).
I only wrote this because I couldn't get that to work with
current Rust in my box, and because I wanted slightly
different functionality.

Specifically, I wrote this library to allow rewriting the
`it_works` test in my reworked version of
[rustastic-password](http://github.com/conradkleinespel/rustastic-password).
Hopefully I can either get my work pushed upstream there or
get a fork published soon.

Many sources of information were used in coding this. They
are listed in the source code.

# License

This work is made available under the "MIT License".  Please
see the file COPYING in this distribution for license terms.
